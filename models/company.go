package models

// Company meta from https://jobs.dou.ua/register/
type Company struct {
	ID            int    `json:"id"`
	Slug          string `json:"slug"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	SiteURL       string `json:"site_url"`
	EmployeeCount int    `json:"employee_count"`
}

type CompaniesNewRequest struct {
	Slug          string `json:"slug"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	SiteURL       string `json:"site_url"`
	EmployeeCount int    `json:"employee_count"`
}

type CompaniesUpdateRequest struct {
	Name          string `json:"name"`
	Description   string `json:"description"`
	SiteURL       string `json:"site_url"`
	EmployeeCount int    `json:"employee_count"`
}

type EmptyResponse = struct{}

type ResponseNew struct {
	ID int `json:"id"`
}
