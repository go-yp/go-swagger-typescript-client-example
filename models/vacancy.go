package models

type Vacancy struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

type VacanciesNewRequest struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type VacanciesUpdateRequest struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type EmployeeVacancyCompany struct {
	ID   int    `json:"id"`
	Slug string `json:"slug"`
	Name string `json:"name"`
}

type EmployeeVacancy struct {
	ID          int                    `json:"id"`
	Title       string                 `json:"title"`
	Description string                 `json:"description"`
	Company     EmployeeVacancyCompany `json:"company"`
}
