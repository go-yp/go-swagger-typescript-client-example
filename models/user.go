package models

const (
	AppUserKey = "app:user"
)

type User struct {
	ID   uint32
	Name string
}
