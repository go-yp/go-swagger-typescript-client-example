package middlewares

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/go-yp/go-swagger-typescript-client-example/models"
	"net/http"
)

func Auth(ctx *gin.Context) {
	// https://developer.mozilla.org/ru/docs/Web/HTTP/Headers/Authorization
	// Authorization header
	{
		var header = ctx.GetHeader("Authorization")

		if header != "" {
			var user *models.User

			// ... logic

			ctx.Set(models.AppUserKey, user)

			ctx.Next()

			return
		}
	}

	{
		var cookie, err = ctx.Cookie("session_id")

		switch err {
		case nil:
			_ = cookie

			var user *models.User

			// ... logic

			ctx.Set(models.AppUserKey, user)

			ctx.Next()

			return
		case http.ErrNoCookie:
			// NOP
		default:
			// log error
		}
	}

	ctx.Next()
}

func User(ctx *gin.Context) (user *models.User, ok bool) {
	var value, exists = ctx.Get(models.AppUserKey)

	if !exists {
		return nil, false
	}

	user, ok = value.(*models.User)
	if ok {
		return user, ok
	}

	return nil, false
}
