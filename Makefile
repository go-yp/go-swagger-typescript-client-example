install-swag:
	go get -u github.com/swaggo/swag/cmd/swag

generate-swagger-docs-001:
	swag init -g ./examples/001_annotations_only/main.go --output ./docs/examples/001_annotations_only/apidocs

generate-swagger-docs-002:
	swag init -g main.go --output ./docs/examples/002_extends/apidocs --exclude ./examples/001_annotations_only

open-swagger-docs-001:
	# docker run -p 80:8080 -e SWAGGER_JSON=/www/swagger.json -v ${PWD}/docs/examples/001_annotations_only/apidocs:/www swaggerapi/swagger-ui
	docker run -p 80:8080 -e SWAGGER_JSON=/www/swagger.json -v ${shell pwd}/docs/examples/001_annotations_only/apidocs:/www swaggerapi/swagger-ui

generate-swagger-ts-client-001:
	# https://openapi-generator.tech/docs/installation/
	# openapi-generator-cli generate -i ./apidocs/swagger.yaml --generator-name typescript-fetch -o gen/api
	# npm run generate-ts-client
	docker run --rm \
      -v $(shell pwd)/docs/examples/001_annotations_only:/local openapitools/openapi-generator-cli generate \
      --skip-validate-spec \
      -g typescript-fetch \
      -i /local/apidocs/swagger.json \
      -o /local/client/gen

generate-swagger-ts-client-002:
	# https://openapi-generator.tech/docs/installation/
	# openapi-generator-cli generate -i ./apidocs/swagger.yaml --generator-name typescript-fetch -o gen/api
	# npm run generate-ts-client
	docker run --rm \
      -v $(shell pwd):/local openapitools/openapi-generator-cli generate \
      --skip-validate-spec \
      -g typescript-fetch \
      -i /local/docs/examples/002_extends/apidocs/swagger.json \
      -o /local/client/gen

swagfmt:
	swag fmt --dir .
	swag fmt --dir ./examples/001_annotations_only

frontend:
	npm run esbuild