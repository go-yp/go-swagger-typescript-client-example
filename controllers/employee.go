package controllers

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"gitlab.com/go-yp/go-swagger-typescript-client-example/models"
	"net/http"
)

type EmployeeController struct {
	connection *sql.DB
}

func NewEmployeeController(connection *sql.DB) *EmployeeController {
	return &EmployeeController{connection: connection}
}

type EmployeeCompaniesQuery struct {
	Page string `form:"page"`
	Size string `form:"size"`
}

// Companies godoc
// @Summary  Show companies
// @Tags     Employee, Companies
// @Accept   json
// @Produce  json
// @Param    page  query     string  false  "Page"
// @Param    size  query     string  false  "Size"
// @Success  200   {object}  []models.Company
// @Failure  400   {object}  models.ErrorResponse
// @Failure  500   {object}  models.ErrorResponse
// @Router   /api/v1/companies [get]
func (c *EmployeeController) Companies(ctx *gin.Context) {
	var query EmployeeCompaniesQuery
	if err := ctx.ShouldBindQuery(&query); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = query.Page
	_ = query.Size

	// ... logic

	// fixtures
	ctx.JSON(http.StatusOK, []models.Company{
		{
			ID:            1,
			Slug:          "softcery",
			Name:          "Softcery",
			Description:   "We build products that create value",
			SiteURL:       "https://softcery.com",
			EmployeeCount: 11,
		},
	})
}

type EmployeeVacanciesQuery struct {
	Page string `form:"page"`
	Size string `form:"size"`
}

// Vacancies godoc
// @Summary  Show vacancies
// @Tags     Employee, Vacancies
// @Accept   json
// @Produce  json
// @Param    page  query     string  false  "Page"
// @Param    size  query     string  false  "Size"
// @Success  200   {object}  []models.EmployeeVacancy
// @Failure  400   {object}  models.ErrorResponse
// @Failure  500   {object}  models.ErrorResponse
// @Router   /api/v1/vacancies [get]
func (c *EmployeeController) Vacancies(ctx *gin.Context) {
	var query EmployeeVacanciesQuery
	if err := ctx.ShouldBindQuery(&query); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = query.Page
	_ = query.Size

	// ... logic

	// fixtures
	ctx.JSON(http.StatusOK, []models.EmployeeVacancy{
		{
			ID:    191265,
			Title: "Go Developer (Remote)",
			Company: models.EmployeeVacancyCompany{
				ID:   1,
				Slug: "softcery",
				Name: "Softcery",
			},
		},
	})
}
