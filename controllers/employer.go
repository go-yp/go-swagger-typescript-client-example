package controllers

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"gitlab.com/go-yp/go-swagger-typescript-client-example/middlewares"
	"gitlab.com/go-yp/go-swagger-typescript-client-example/models"
	"net/http"
)

type EmployerController struct {
	connection *sql.DB
}

func NewEmployerController(connection *sql.DB) *EmployerController {
	return &EmployerController{connection: connection}
}

type EmployerCompaniesQuery struct {
	Page string `form:"page"`
	Size string `form:"size"`
}

// Companies godoc
// @Summary  Show companies
// @Tags     Employer, Companies
// @Accept   json
// @Produce  json
// @Param    page  query     string  false  "Page"
// @Param    size  query     string  false  "Size"
// @Success  200   {object}  []models.Company
// @Failure  400   {object}  models.ErrorResponse
// @Failure  401   {object}  models.ErrorResponse
// @Failure  500   {object}  models.ErrorResponse
// @Router   /api/v1/hire/companies [get]
func (c *EmployerController) Companies(ctx *gin.Context) {
	var user, ok = middlewares.User(ctx)

	_ = user
	_ = ok

	if false && !ok {
		ctx.JSON(http.StatusUnauthorized, &models.ErrorResponse{
			Message: "Unauthorized",
		})

		return
	}

	var query EmployerCompaniesQuery
	if err := ctx.ShouldBindQuery(&query); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = query.Page
	_ = query.Size

	// ... logic

	ctx.JSON(http.StatusInternalServerError, &models.ErrorResponse{
		Message: "Unimplemented",
	})
}

// CompaniesNew godoc
// @Summary  Add company
// @Tags     Employer, Companies
// @Accept   json
// @Produce  json
// @Param    body  body      models.CompaniesNewRequest  true  "CompaniesNewRequest"
// @Success  201   {object}  models.ResponseNew
// @Failure  400   {object}  models.ErrorResponse
// @Failure  401   {object}  models.ErrorResponse
// @Failure  500   {object}  models.ErrorResponse
// @Router   /api/v1/hire/companies [post]
func (c *EmployerController) CompaniesNew(ctx *gin.Context) {
	var user, ok = middlewares.User(ctx)

	_ = user
	_ = ok

	if false && !ok {
		ctx.JSON(http.StatusUnauthorized, &models.ErrorResponse{
			Message: "Unauthorized",
		})

		return
	}

	var companyRequest models.CompaniesNewRequest
	if err := ctx.ShouldBindJSON(&companyRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	// ... logic

	ctx.JSON(http.StatusInternalServerError, &models.ErrorResponse{
		Message: "Unimplemented",
	})
}

type CompanyURI struct {
	CompanyID int `uri:"company_id" binding:"required"`
}

// CompaniesGetOne godoc
// @Summary  Get one company by id
// @Tags     Employer, Companies
// @Accept   json
// @Produce  json
// @Param    company_id  path      integer  true  "Company ID"
// @Success  200         {object}  models.Company
// @Failure  400         {object}  models.ErrorResponse
// @Failure  401         {object}  models.ErrorResponse
// @Failure  404         {object}  models.ErrorResponse
// @Failure  500         {object}  models.ErrorResponse
// @Router   /api/v1/hire/companies/{company_id} [get]
func (c *EmployerController) CompaniesGetOne(ctx *gin.Context) {
	var user, ok = middlewares.User(ctx)

	_ = user
	_ = ok

	if false && !ok {
		ctx.JSON(http.StatusUnauthorized, &models.ErrorResponse{
			Message: "Unauthorized",
		})

		return
	}

	var uri CompanyURI
	if err := ctx.ShouldBindUri(&uri); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = uri.CompanyID

	// ... logic

	ctx.JSON(http.StatusInternalServerError, &models.ErrorResponse{
		Message: "Unimplemented",
	})
}

// CompaniesUpdate godoc
// @Summary  Update company
// @Tags     Employer, Companies
// @Accept   json
// @Produce  json
// @Param    company_id  path      integer                        true  "Company ID"
// @Param    body        body      models.CompaniesUpdateRequest  true  "CompaniesUpdateRequest"
// @Success  200         {object}  models.Company
// @Failure  400         {object}  models.ErrorResponse
// @Failure  401         {object}  models.ErrorResponse
// @Failure  404         {object}  models.ErrorResponse
// @Failure  500         {object}  models.ErrorResponse
// @Router   /api/v1/hire/companies/{company_id} [post]
func (c *EmployerController) CompaniesUpdate(ctx *gin.Context) {
	var user, ok = middlewares.User(ctx)

	_ = user
	_ = ok

	if false && !ok {
		ctx.JSON(http.StatusUnauthorized, &models.ErrorResponse{
			Message: "Unauthorized",
		})

		return
	}

	var uri CompanyURI
	if err := ctx.ShouldBindUri(&uri); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = uri.CompanyID

	var companyRequest models.CompaniesUpdateRequest
	if err := ctx.ShouldBindJSON(&companyRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	// ... logic

	ctx.JSON(http.StatusInternalServerError, &models.ErrorResponse{
		Message: "Unimplemented",
	})
}

type EmployerVacanciesQuery struct {
	Page string `form:"page"`
	Size string `form:"size"`
}

// Vacancies godoc
// @Summary  Show vacancies
// @Tags     Employer, Vacancies
// @Accept   json
// @Produce  json
// @Param    page  query     string  false  "Page"
// @Param    size  query     string  false  "Size"
// @Success  200   {object}  []models.Vacancy
// @Failure  400   {object}  models.ErrorResponse
// @Failure  401   {object}  models.ErrorResponse
// @Failure  404   {object}  models.ErrorResponse
// @Failure  500   {object}  models.ErrorResponse
// @Router   /api/v1/hire/companies/{company_id}/vacancies [get]
func (c *EmployerController) Vacancies(ctx *gin.Context) {
	var user, ok = middlewares.User(ctx)

	_ = user
	_ = ok

	if false && !ok {
		ctx.JSON(http.StatusUnauthorized, &models.ErrorResponse{
			Message: "Unauthorized",
		})

		return
	}

	var uri CompanyURI
	if err := ctx.ShouldBindUri(&uri); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = uri.CompanyID

	var query EmployerVacanciesQuery
	if err := ctx.ShouldBindQuery(&query); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = query.Page
	_ = query.Size

	// ... logic

	ctx.JSON(http.StatusInternalServerError, &models.ErrorResponse{
		Message: "Unimplemented",
	})
}

// VacanciesNew godoc
// @Summary  Add vacancy
// @Tags     Employer, Vacancies
// @Accept   json
// @Produce  json
// @Param    body  body      models.VacanciesNewRequest  true  "VacanciesNewRequest"
// @Success  201   {object}  models.ResponseNew
// @Failure  400   {object}  models.ErrorResponse
// @Failure  401   {object}  models.ErrorResponse
// @Failure  404   {object}  models.ErrorResponse
// @Failure  500   {object}  models.ErrorResponse
// @Router   /api/v1/hire/companies/{company_id}/vacancies [post]
func (c *EmployerController) VacanciesNew(ctx *gin.Context) {
	var user, ok = middlewares.User(ctx)

	_ = user
	_ = ok

	if false && !ok {
		ctx.JSON(http.StatusUnauthorized, &models.ErrorResponse{
			Message: "Unauthorized",
		})

		return
	}

	var uri CompanyURI
	if err := ctx.ShouldBindUri(&uri); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = uri.CompanyID

	var vacancyRequest models.VacanciesNewRequest
	if err := ctx.ShouldBindJSON(&vacancyRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	// ... logic

	ctx.JSON(http.StatusInternalServerError, &models.ErrorResponse{
		Message: "Unimplemented",
	})
}

type VacancyURI struct {
	CompanyID int `uri:"company_id" binding:"required"`
	VacancyID int `uri:"vacancy_id" binding:"required"`
}

// VacanciesGetOne godoc
// @Summary  Get one vacancy by id
// @Tags     Employer, Vacancies
// @Accept   json
// @Produce  json
// @Param    company_id  path      integer  true  "Company ID"
// @Param    vacancy_id  path      integer  true  "Vacancy ID"
// @Success  200         {object}  models.Vacancy
// @Failure  400         {object}  models.ErrorResponse
// @Failure  401         {object}  models.ErrorResponse
// @Failure  404         {object}  models.ErrorResponse
// @Failure  500         {object}  models.ErrorResponse
// @Router   /api/v1/hire/companies/{company_id}/vacancies/{vacancy_id} [get]
func (c *EmployerController) VacanciesGetOne(ctx *gin.Context) {
	var user, ok = middlewares.User(ctx)

	_ = user
	_ = ok

	if false && !ok {
		ctx.JSON(http.StatusUnauthorized, &models.ErrorResponse{
			Message: "Unauthorized",
		})

		return
	}

	var uri VacancyURI
	if err := ctx.ShouldBindUri(&uri); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = uri.CompanyID
	_ = uri.VacancyID

	// ... logic

	ctx.JSON(http.StatusInternalServerError, &models.ErrorResponse{
		Message: "Unimplemented",
	})
}

// VacanciesUpdate godoc
// @Summary  Update vacancy
// @Tags     Employer, Vacancies
// @Accept   json
// @Produce  json
// @Param    company_id  path      integer                        true  "Company ID"
// @Param    vacancy_id  path      integer                        true  "Vacancy ID"
// @Param    body        body      models.VacanciesUpdateRequest  true  "VacanciesUpdateRequest"
// @Success  200         {object}  models.Vacancy
// @Failure  400         {object}  models.ErrorResponse
// @Failure  401         {object}  models.ErrorResponse
// @Failure  404         {object}  models.ErrorResponse
// @Failure  500         {object}  models.ErrorResponse
// @Router   /api/v1/hire/companies/{company_id}/vacancies/{vacancy_id} [post]
func (c *EmployerController) VacanciesUpdate(ctx *gin.Context) {
	var user, ok = middlewares.User(ctx)

	_ = user
	_ = ok

	if false && !ok {
		ctx.JSON(http.StatusUnauthorized, &models.ErrorResponse{
			Message: "Unauthorized",
		})

		return
	}

	var uri VacancyURI
	if err := ctx.ShouldBindUri(&uri); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	_ = uri.CompanyID
	_ = uri.VacancyID

	var vacancyRequest models.VacanciesUpdateRequest
	if err := ctx.ShouldBindJSON(&vacancyRequest); err != nil {
		ctx.JSON(http.StatusBadRequest, &models.ErrorResponse{
			Message: err.Error(),
		})

		return
	}

	// ... logic

	ctx.JSON(http.StatusInternalServerError, &models.ErrorResponse{
		Message: "Unimplemented",
	})
}
