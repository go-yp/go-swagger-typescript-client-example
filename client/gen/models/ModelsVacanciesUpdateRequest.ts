/* tslint:disable */
/* eslint-disable */
/**
 * 
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

import { exists, mapValues } from '../runtime';
/**
 * 
 * @export
 * @interface ModelsVacanciesUpdateRequest
 */
export interface ModelsVacanciesUpdateRequest {
    /**
     * 
     * @type {string}
     * @memberof ModelsVacanciesUpdateRequest
     */
    description?: string;
    /**
     * 
     * @type {string}
     * @memberof ModelsVacanciesUpdateRequest
     */
    name?: string;
}

export function ModelsVacanciesUpdateRequestFromJSON(json: any): ModelsVacanciesUpdateRequest {
    return ModelsVacanciesUpdateRequestFromJSONTyped(json, false);
}

export function ModelsVacanciesUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ModelsVacanciesUpdateRequest {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        
        'description': !exists(json, 'description') ? undefined : json['description'],
        'name': !exists(json, 'name') ? undefined : json['name'],
    };
}

export function ModelsVacanciesUpdateRequestToJSON(value?: ModelsVacanciesUpdateRequest | null): any {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        
        'description': value.description,
        'name': value.name,
    };
}

