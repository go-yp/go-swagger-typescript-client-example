import {VacanciesApi, Configuration, ModelsEmployeeVacancy} from "./gen";

const vacanciesApiClient = new VacanciesApi(new Configuration({
    basePath: "http://localhost:8080",
}));

vacanciesApiClient.apiV1VacanciesGet()
    .then(function (vacancies: ModelsEmployeeVacancy[]) {
        for (const vacancy of vacancies) {
            console.log(
                vacancy.id,
                vacancy.title,
                vacancy.description,

                vacancy.company.id,
                vacancy.company.slug,
                vacancy.company.name,
            );
        }
    })
    .catch(console.error)