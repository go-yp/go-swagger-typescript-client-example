import {CompaniesApi, Configuration, ModelsCompany} from "./gen";

const companiesApiClient = new CompaniesApi(new Configuration({
    basePath: "http://localhost:8080",
}));

companiesApiClient.apiV1CompaniesGet()
    .then(function (companies: ModelsCompany[]) {
        for (const company of companies) {
            console.log(
                company.id,
                company.slug,
                company.name,
                company.description,
                company.siteUrl,
                company.employeeCount,
            );
        }
    })
    .catch(console.error)