package main

// Company meta from https://jobs.dou.ua/register/
type Company struct {
	ID            int    `json:"id"`
	Slug          string `json:"slug"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	SiteURL       string `json:"site_url"`
	EmployeeCount int    `json:"employee_count"`
}

type ErrorResponse struct {
	Message string `json:"message"`
}

// Companies godoc
// @Summary  Show companies
// @Tags     Companies
// @Accept   json
// @Produce  json
// @Param    page  query     string  false  "Page"
// @Param    size  query     string  false  "Size"
// @Success  200   {object}  []Company
// @Failure  400   {object}  ErrorResponse
// @Failure  401   {object}  ErrorResponse
// @Failure  404   {object}  ErrorResponse
// @Failure  500   {object}  ErrorResponse
// @Router   /api/v1/hire/companies [get]
func Companies() {
	// NOP
}

type ResponseNew struct {
	ID int `json:"id"`
}

type CompaniesNewRequest struct {
	Slug          string `json:"slug"`
	Name          string `json:"name"`
	Description   string `json:"description"`
	SiteURL       string `json:"site_url"`
	EmployeeCount int    `json:"employee_count"`
}

// CompaniesNew godoc
// @Summary  Add company
// @Tags     Companies
// @Accept   json
// @Produce  json
// @Param    body  body      CompaniesNewRequest  true  "CompaniesNewRequest"
// @Success  200   {object}  ResponseNew
// @Failure  401   {object}  ErrorResponse
// @Failure  404   {object}  ErrorResponse
// @Failure  500   {object}  ErrorResponse
// @Router   /api/v1/hire/companies [post]
func CompaniesNew() {
	// NOP
}

// CompaniesGetByID godoc
// @Summary  Get one company by id
// @Tags     Companies
// @Accept   json
// @Produce  json
// @Param    id   path      integer  true  "id"
// @Success  200  {object}  Company
// @Failure  401  {object}  ErrorResponse
// @Failure  404  {object}  ErrorResponse
// @Failure  500  {object}  ErrorResponse
// @Router   /api/v1/hire/companies/{id} [get]
func CompaniesGetByID() {
	// NOP
}

type EmptyResponse = struct{}

type CompaniesUpdateRequest struct {
	Name          string `json:"name"`
	Description   string `json:"description"`
	SiteURL       string `json:"site_url"`
	EmployeeCount int    `json:"employee_count"`
}

// CompaniesUpdate godoc
// @Summary  Update company
// @Tags     Companies
// @Accept   json
// @Produce  json
// @Param    id    path      integer                 true  "id"
// @Param    body  body      CompaniesUpdateRequest  true  "CompaniesUpdateRequest"
// @Success  200   {object}  EmptyResponse
// @Failure  401   {object}  ErrorResponse
// @Failure  404   {object}  ErrorResponse
// @Failure  500   {object}  ErrorResponse
// @Router   /api/v1/hire/companies/{id} [post]
func CompaniesUpdate() {
	// NOP
}

func main() {
	// NOP
}
