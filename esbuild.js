require("esbuild").buildSync({
    entryPoints: [
        "./client/companies-app.ts",
        "./client/vacancies-app.ts",
    ],
    bundle: true,
    minify: true,
    outdir: "./public/assets/js",
});